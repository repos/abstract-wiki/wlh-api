#!/usr/bin/env bash

if [[ -f .tunnel_pid ]]; then
	kill $(cat .tunnel_pid)
	rm .tunnel_pid
else
	echo tunnel is not running
fi
