user := `whoami`

build:
  cargo build

run toolforge_user=user: (db-tunnel toolforge_user)
  cargo run

db-tunnel toolforge_user=user:
  scripts/replica_db_tunnel.sh {{toolforge_user}}

kill-tunnel:
  scripts/kill_tunnel.sh

db toolforge_user=user: (db-tunnel toolforge_user)
  mysql -u $MYSQL_USER --password=$MYSQL_PASS -P $MYSQL_PORT enwiki_p

show-db-creds toolforge_user=user:
  ssh {{toolforge_user}}@login.toolforge.org become wlh-api cat replica.my.cnf

deploy toolforge_user=user:
  scripts/deploy_to_toolforge.sh {{toolforge_user}}
